# WinBio - Web-Based Windows 98 Emulator

**WinBio** is a nostalgic project that emulates the iconic Windows 98 operating system on the web. Built with _Create React App_ and Tailwind CSS, it offers an immersive experience with features like multiple windows, a web browser, an MS-DOS Terminal emulator, and a personalized bio section.

## Features

- 🖥️ Web-based Windows 98 Emulator
- 🌐 Accessible from anywhere with an internet connection
- 📂 Customized bio and information section
- 🖼️ Multiple windows for a multitasking experience
- 🌐 Web browser to explore the internet (limited functionality)
- 💻 MS-DOS Terminal emulator for a command-line interface
- 🎮 Nostalgic interface for a trip down memory lane
- 🚀 Built with React and Tailwind CSS for a smooth and interactive user experience

## Demo

Check out the live demo of WinBio [here](https://winbio.subinoy.me) and experience the retro vibes of Windows 98!

Or, checkout workings of WinBio [here](https://youtu.be/avIjvkXNeJ4?si=Jm3-ySJtwwc11L9g)

## Screenshots

![Screenshot 1](screenshots/screenshot1.png)
_Win-Bio with MS-DOS open_

![Screenshot 2](screenshots/screenshot2.png)
_Win-Bio with multiple windows open_

## Prerequisites

Before you begin, make sure you have [Yarn](https://yarnpkg.com/) installed. If not, you can install it by following the instructions [here](https://yarnpkg.com/getting-started/install).

## Getting Started

To run WinBio locally, follow these steps:

1. Clone this repository: `git clone https://github.com/subinoybiswas/WinBio.git`
2. Navigate to the project directory: `cd WinBio/desktop-portfolio`
3. Install dependencies: `yarn install`
4. Start the development server: `yarn start`
5. Open your browser and visit `http://localhost:3000`

## Credits

This project was inspired by Don Chia's Portfolio built in Vue.js. Checkout his work [here](https://www.donchia.tech/)

## Contributing

If you'd like to contribute to WinBio, feel free to open an issue or submit a pull request. All contributions are welcome!

